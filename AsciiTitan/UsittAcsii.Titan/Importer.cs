﻿using System;
using Avolites.Acw;
using Avolites.Acw.Titan;
using Avolites.Acw.Titan.Clients;
using UsittAsciiGeneric.Data;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Text.RegularExpressions;
using Avolites.Acw.Titan.Entities;
using System.Text;
using System.Xml;

namespace UsittAcsii.Titan
{

    public class ImportOptions
    {
        public enum UnpatchedChannels
        {
            Skip,
            PatchParked,
            PatchAtChannel
        }

        public UnpatchedChannels UnpatchedChannelBehaviour { get; set; } = UnpatchedChannels.PatchParked;

        public bool StartNewShow { get; set; } = true;

        public string Address { get; set; } = "localhost";
    }
    public class Importer
    {
        public Importer()
        {

        }

        public FixturesClient TitanFixtures { get; private set; }
        public GroupsClient TitanGroups { get; private set; }
        public ProgrammerClient TitanProgrammer { get; private set; }
        public AcwClientId ClientId { get; private set; }
        public ContextProgrammerCollection Programmers { get; private set; }
        public CueLibraryClient TitanCueLibrary { get; private set; }
        public CurveLibraryClient TitanCurveLibrary { get; private set; }

        AcwProgrammerIdPair Programmer => Programmers.LiveProgrammer;

        public NullShowClient TitanShow { get; private set; }
        public GenericEntityStoreClient TitanEntityStore { get; private set; }

        public void BuildShow(DataManager show, ImportOptions options)
        {
            var binding = Avolites.Acw.BindingHelper.CreateDefaultTcpBinding();
            AcwSessionManager<ITitanHub, TitanHubClient> session = new AcwSessionManager<ITitanHub, TitanHubClient>(
    "net.tcp://" + options.Address + "/Titan/", new AcwClientDescription<ITitanHub, TitanHubClient>(binding, "Hub"),
    (arg) => arg.DeviceInfo != null);

            session.UnhandledException += (s, e) => { Console.WriteLine(((Exception)e.ExceptionObject).Message); };
            //this.acwManager.Connected += new EventHandler(this.acwManager_Connected);
            //this.acwManager.Disconnected += new EventHandler(this.acwManager_Disconnected);
            session.RegisterClient(new AcwClientDescription<ICueLibrary, CueLibraryClient>(binding, "CueLibrary"));
            session.RegisterClient(new AcwClientDescription<IPaletteLibrary, PaletteLibraryClient>(binding, "PaletteLibrary"));
            session.RegisterClient(new AcwClientDescription<IFixtures, FixturesClient, IFixturesStatusEvents>(binding, "Fixtures"));
            session.RegisterClient(new AcwClientDescription<IProgrammer, ProgrammerClient>(binding, "Programmer"));
            session.RegisterClient(new AcwClientDescription<IGroups, GroupsClient>(binding, "Groups"));
            session.RegisterClient(new AcwClientDescription<IShow, ShowClient, IShowSectionEvents>(binding, "Show"));
            session.RegisterClient(new AcwClientDescription<ICurveLibrary, CurveLibraryClient>(binding, "CurveLibrary"));
            session.RegisterClient(new AcwClientDescription<IGenericEntityStore, GenericEntityStoreClient, IGenericEntityNotificationsEvents>(binding, "Entities"));

            TitanFixtures = session.GetClient<FixturesClient, IFixtures>();
            TitanGroups = session.GetClient<GroupsClient, IGroups>();
            TitanProgrammer = session.GetClient<ProgrammerClient, IProgrammer>();
            TitanCueLibrary = session.GetClient<CueLibraryClient, ICueLibrary>();
            TitanShow = session.GetClient<NullShowClient, IShow>();
            TitanCurveLibrary = session.GetClient<CurveLibraryClient, ICurveLibrary>();
            TitanEntityStore = session.GetClient<GenericEntityStoreClient, IGenericEntityStore>();
            session.Start(true);


            ClientId = new AcwClientId("UsittAsciiShowImporter - " + Guid.NewGuid());

            if (options.StartNewShow)
            {
                ManualResetEvent newFinished = new ManualResetEvent(false);
                TitanShow.LoadFinished += (o, e) => { newFinished.Set(); };
                TitanShow.AfterNew += (o, e) => { newFinished.Set(); };
                TitanShow.LoadError += (o, e) => { newFinished.Set(); };
                TitanShow.RegisterShowSection("UsittAcsiiImport", AcwShowSectionRoles.MultiMaster, ClientId);
                TitanShow.NewShow(new Avolites.Acw.Titan.Show.NewShowInformation()
                {
                    NewShowType = Avolites.Acw.Titan.Show.AcwNewShowType.WipeAllNewShow,
                    WipeOptions = Avolites.Acw.Titan.Show.AcwShowParts.All,
                    Show = new Avolites.Acw.Titan.Show.AcwShowInformation()
                    {
                        Name = show.ShowName
                    }
                },
                ClientId);

                newFinished.WaitOne();
            }


            AcwTransactionToken token = AcwTransactionToken.Create(ClientId, 0, "Import ASCII show file");
            Programmers = TitanProgrammer.GetPanelProgrammerPlaybacks(ClientId);


            HashSet<int> allUsedChannels = new HashSet<int>();
            allUsedChannels.UnionWith(show.Groups.SelectMany(g => g.Levels.Keys));
            allUsedChannels.UnionWith(show.Subs.SelectMany(g => g.Levels.Keys));
            allUsedChannels.UnionWith(show.Cues.SelectMany(g => g.Levels.Keys));

            Dictionary<int, List<int>> fixtures = new Dictionary<int, List<int>>();

            Regex regex = new Regex(@"(\d+)% Linear");
            SortedDictionary<float, CurveInformation> curveCache = new SortedDictionary<float, CurveInformation>();
            var curves = TitanCurveLibrary.Curves;
            foreach (var curve in curves)
            {
                var match = regex.Match(curve.Name);
                if (match.Success)
                {
                    int hundredRangeLevel;
                    if (int.TryParse(match.Groups[1].Value, out hundredRangeLevel))
                    {
                        float level = hundredRangeLevel / 100f;
                        curveCache.Add(level, curve);
                    }
                }
            }

            var availableDimmerLevels = curveCache.Keys.ToList();

            foreach (var fixture in show.Fixtures.OrderBy(f => f.Channel))
            {
                int fixtureId = Patch(token, fixtures, fixture.Channel, new DmxAssignment(fixture.Address));
                if (fixture.Level < 1)
                {
                    int index = availableDimmerLevels.BinarySearch(fixture.Level);
                    if (index < 0)
                    {
                        // Nearest match
                        index = ~index;
                    }
                    CurveInformation curveInfo = null;
                    if (index <= availableDimmerLevels.Count)
                    {
                        float nearest = availableDimmerLevels[index];
                        curveInfo = curveCache[nearest];
                    }

                    if (curveInfo != null)
                    {
                        TitanFixtures.SetFixtureControlCurve(new List<int>() { fixtureId }, (int)FixtureControlId.Dimmer, curveInfo.Name);
                    }
                }
                allUsedChannels.Remove(fixture.Channel);
            }

            switch (options.UnpatchedChannelBehaviour)
            {
                case ImportOptions.UnpatchedChannels.Skip:
                    foreach (int channel in allUsedChannels)
                    {
                        fixtures[channel] = new List<int>();
                    }
                    break;

                case ImportOptions.UnpatchedChannels.PatchParked:
                    foreach (int channel in allUsedChannels)
                    {
                        Patch(token, fixtures, channel, DmxAssignment.Parked);
                    }
                    break;

                case ImportOptions.UnpatchedChannels.PatchAtChannel:
                    foreach (int channel in allUsedChannels)
                    {
                        Patch(token, fixtures, channel, new DmxAssignment(channel - 1));
                    }
                    break;
            }

            foreach (var group in show.Groups.OrderBy(g => g.Number))
            {
                int groupId = TitanGroups.StoreGroup(token, group.Name,
                                       group.Levels.Keys.SelectMany(chan => fixtures[chan]).ToList(),
                                       AcwProgrammerIdPair.Empty);
                var userNumber = new AcwUserNumber((int)group.Number);
                if (!TitanGroups.DoesUserNumberExist(userNumber))
                {
                    TitanGroups.SetGroupUserNumber(token, groupId, userNumber);
                }
            }

            foreach (var sub in show.Subs.OrderBy(s => s.Number))
            {
                SetLevelsInProgrammer(fixtures, sub);

                int playbackId = TitanCueLibrary.StorePlayback(
                    token,
                    Programmer,
                    AcwPlaybackType.PlaybackTypeCue,
                    AcwRecordMode.RecordCueModeFixture,
                    mask: AcwRecordMask.All,
                    forceStoreGroups: AcwRecordMask.Empty,
                    aliasControlsGroups: AcwRecordMask.Empty,
                    recordPaletteReferences: true,
                    clientId: ClientId);

                if (!string.IsNullOrWhiteSpace(sub.Name))
                {
                    TitanCueLibrary.SetPlaybackPropertyValue(
                        token,
                        playbackId,
                        new TitanProperty<PlaybackProperties>(PlaybackProperties.Legend, sub.Name)
                    );
                }

                var cueInfo = TitanCueLibrary.GetPlaybackMemory(playbackId);
                int cueId = cueInfo.Id;

                SetCueTime(token, sub, playbackId, cueId);
            }

            if (show.Cues.Any())
            {
                Clear();
                int playbackId = TitanCueLibrary.StoreEmptyPlayback(
                    token,
                    AcwPlaybackType.PlaybackTypeCueList,
                    ClientId);

                TimeSpan? followOn = null;

                foreach (var cue in show.Cues.OrderBy(c => c.Number))
                {
                    SetLevelsInProgrammer(fixtures, cue);
                    int cueId = TitanCueLibrary.StorePlaybackCue(
                        token,
                        Programmer,
                        playbackId,
                        AcwRecordMode.RecordCueModeFixture,
                        mask: AcwRecordMask.All,
                        cueNumber: (float)cue.Number,
                            clientId: ClientId);

                    if (!string.IsNullOrWhiteSpace(cue.Name))
                    {

                        TitanCueLibrary.SetPlaybackCuePropertyValue(
                          token,
                          playbackId,
                           cueId,
                            new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.Legend, cue.Name));
                    }

                    SetCueTime(token, cue, playbackId, cueId);

                    // Titan's cue linking is rather different to strands.
                    // The 'follow on' is set in the cue that will go not the
                    // cue that is followed so we have to push this forward
                    if (followOn.HasValue)
                    {
                        TitanCueLibrary.SetPlaybackCuePropertyValue(
                            token,
                            playbackId,
                             cueId,
                              new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.Link, AcwLinkType.LinkWithPrevious));
                        TitanCueLibrary.SetPlaybackCuePropertyValue(
                             token,
                             playbackId,
                              cueId,
                               new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.LinkOffsetTime, new AcwTimeSpan(followOn.Value)));


                    }

                    followOn = cue.FollowOn;

                    if (cue.LinkedCue.HasValue)
                    {
                        Avolites.Menus.Macros.Macro macro = new Avolites.Menus.Macros.Macro();
                        var step = new Avolites.Menus.Macros.MacroStep();
                        step.Script = $"CueLists.SetNextCue(Handles.GetHandleFromId(\"cueListHandle\", {playbackId}), float:{cue.LinkedCue.Value})";
                        macro.Steps.Add(step);
                        macro.Id = $"UserMacro.Link{playbackId}To{cue.LinkedCue.Value}";
                        macro.Name = $"Link to Cue {cue.LinkedCue.Value}";
                        var userMacroEntity = new AcwGenericEntity()
                        {
                            Legend = macro.Name
                        };
                        StringBuilder builder = new StringBuilder();
                        using (XmlWriter writer = XmlWriter.Create(builder, new XmlWriterSettings() { ConformanceLevel = ConformanceLevel.Fragment, OmitXmlDeclaration = true }))
                        {
                            // Don't know why on earth macro doesn't write its own element?!
                            writer.WriteStartElement("macro");
                            macro.SaveXml(writer);
                            writer.WriteEndElement();
                        }
                        userMacroEntity.Properties["MacroData"] = builder.ToString();
                        userMacroEntity.Properties["CreatedBy"] = ClientId.ToString();
                        int macroTitanId = TitanEntityStore.AddEntity(token, "UserMacro", userMacroEntity, true);

                        TitanCueLibrary.AddMacroLink(token, playbackId, cueId, macro.Id);

                    }
                }
            }

            session.Disconnect();
            session.Dispose();
        }

        private int Patch(AcwTransactionToken token, Dictionary<int, List<int>> fixtures, int channel, DmxAssignment address)
        {
            List<int> fixtureIds;
            if (!fixtures.TryGetValue(channel, out fixtureIds))
            {
                fixtureIds = new List<int>();
                fixtures[channel] = fixtureIds;
            }
            int fixtureId =
                TitanFixtures.PatchFixture(token,
                                           "Generic", "Dimmer", "Dimmer",
                                           address,
                                           new AcwUserNumber(channel));
            fixtureIds.Add(fixtureId);
            return fixtureId;
        }

        private void SetCueTime(AcwTransactionToken token, PrimaryTimes sub, int playbackId, int cueId)
        {
            if (sub.FadeUpTime.HasValue)
            {
                TitanCueLibrary.SetPlaybackCuePropertyValue(
                   token,
                   playbackId,
                    cueId,
                    new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.Fade,
                                                             new AcwTimeSpan(sub.FadeUpTime.Value))
               );
            }

            if (sub.FadeDownTime.HasValue)
            {
                TitanCueLibrary.SetPlaybackCuePropertyValue(
                   token,
                   playbackId,
                    cueId,
                    new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.FadeOut,
                                                             new AcwTimeSpan(sub.FadeDownTime.Value))
               );

            }

            if (sub.DelayUpTime.HasValue)
            {
                TitanCueLibrary.SetPlaybackCuePropertyValue(
                   token,
                   playbackId,
                    cueId,
                    new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.Delay,
                                                             new AcwTimeSpan(sub.DelayUpTime.Value))
               );
            }

            if (sub.DelayDownTime.HasValue)
            {
                TitanCueLibrary.SetPlaybackCuePropertyValue(
                   token,
                   playbackId,
                    cueId,
                    new TitanProperty<PlaybackCueProperties>(PlaybackCueProperties.DelayOut,
                                                             new AcwTimeSpan(sub.DelayDownTime.Value))
               );
            }


        }

        private void SetLevelsInProgrammer(Dictionary<int, List<int>> fixtures, PrimaryChannels sub)
        {
            Clear();
            foreach (var channel in sub.Levels.OrderBy(c => c.Key))
            {
                TitanFixtures.SetFixtureControlCurrentValue(
                    Programmer,
                    fixtures[channel.Key],
                    (int)FixtureControlId.Dimmer,
                    new ControlValue(1, AcwRangeValue.FromScaledValue(channel.Value)),
                    true,
                    ClientId);
            }
        }

        private void Clear()
        {
            TitanProgrammer.ClearProgrammer(ClearOptions.ClearAll(Programmer), ClientId);
        }
    }
}
