﻿using System;
namespace UsittAsciiGeneric.Data
{
    public class Fixture
    {
        public int Channel
        {
            get;
            set;
        }

        public int Address
        {
            get;
            set;
        }

        public float Level
        {
            get;
            set;
        }
    }
}
