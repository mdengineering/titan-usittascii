﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Data
{
    public class DataManager
    {
   
        public static PrimaryBase EmptyContext = new PrimaryBase();


        Dictionary<Decimal, Cue> cues = new Dictionary<Decimal, Cue>();

        public IEnumerable<Cue> Cues => cues.Values;

        public void AddCue(Cue cue)
        {
            cues[cue.Number] = cue;
        }

        Dictionary<int, Sub> subs = new Dictionary<int, Sub>();

        public IEnumerable<Sub> Subs => subs.Values;

        internal void AddSub(Sub sub)
        {
            subs[sub.Number] = sub;
        }

        Dictionary<Decimal, Group> groups = new Dictionary<Decimal, Group>();

        public IEnumerable<Group> Groups => groups.Values;

		internal void AddGroup(Group group)
        {
            groups[group.Number] = group;
        }

        List<Fixture> fixtures = new List<Fixture>();

        public IEnumerable<Fixture> Fixtures => fixtures;

        public string ShowName { get; set; }

        internal void Patch(int channel, int address, float level)
        {
            fixtures.Add(new Fixture() { Channel = channel, Address = address, Level = level});
        }

       
    }
}
