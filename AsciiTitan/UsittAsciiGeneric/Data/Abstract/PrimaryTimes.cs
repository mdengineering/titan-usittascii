﻿using System;
namespace UsittAsciiGeneric.Data
{
    public class PrimaryTimes : PrimaryChannels
    {
        public PrimaryTimes()
        {
        }

		public TimeSpan? FadeUpTime { get; set; }
		public TimeSpan? DelayUpTime { get; set; }

		internal override void Apply(Expressions.UpExpression expr) { FadeUpTime = expr.FadeTime; DelayUpTime = expr.DelayTime; }

		public TimeSpan? FadeDownTime { get; set; }
		public TimeSpan? DelayDownTime { get; set; }

		internal override void Apply(Expressions.DownExpression expr) { FadeDownTime = expr.FadeTime; DelayDownTime = expr.DelayTime; }


	}
}
