﻿using System;
using UsittAsciiGeneric.Expressions;

namespace UsittAsciiGeneric.Data
{
    public class PrimaryBase
    {
        public string Name
        {
            get;
            set;
        }

        internal virtual void Apply(TextExpression expr) => Name = expr.Text;
  
		internal virtual void Apply(UpExpression expr) {}

		internal virtual void Apply(DownExpression expr) { }

        internal virtual void Apply(ChannelExpression expr) { }
		internal virtual void Apply(FollowOnExpression expr) { }
        internal virtual void Apply(LinkExpression expr) { }

	}
}
