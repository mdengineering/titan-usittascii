﻿using System;
using System.Collections.Generic;

namespace UsittAsciiGeneric.Data
{
    public class PrimaryChannels : PrimaryBase
    {
        public PrimaryChannels()
        {
        }

		public Dictionary<int, float> Levels
		{
			get;
			set;
		} = new Dictionary<int, float>();

		internal override void Apply(Expressions.ChannelExpression expr)
		{
			Levels[expr.Channel] = expr.Level;
		}
    }
}
