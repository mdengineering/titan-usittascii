﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsittAsciiGeneric.Expressions;

namespace UsittAsciiGeneric.Data
{
    public class Cue : PrimaryTimes
    {
        public Decimal Number
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the follow on time.
        /// </summary>
        /// <remarks>
        /// If the CUE record collection contains a FOLLOWON keyword, 
        /// then the data in that record shall set the interval from 
        /// initiation of the current cue until initiation of the next cue. 
        /// The current cue is the one described by the current CUE record 
        /// collection. When the FOLLOWON keyword is present, initiation 
        /// of the next cue shall occur automatically (after the specified interval). 
        /// If the FOLLOWON keyword is absent, the next cue shall be initiated by 
        /// some manual action
        /// </remarks>
        /// <value>
        /// The follow on.
        /// </value>
        public TimeSpan? FollowOn { get; set; }

        internal override void Apply(FollowOnExpression expr)
        {
            FollowOn = expr.Time;
        }

        /// <summary>
        /// Gets or sets the linked cue.
        /// </summary>
        /// <remarks>
        /// A LINK keyword record shall name next cue executed following
        /// the current cue. The current cue is the one described by the 
        /// CUE record collection in which the LINK keyword appears. 
        /// If a CUE record collection contains no LINK keyword, 
        /// then the next higher numbered cue shall be the next cue executed
        /// </remarks>
        /// <value>
        /// The linked cue.
        /// </value>
        public Decimal? LinkedCue { get; set; }

        internal override void Apply(LinkExpression expr)
        {
            LinkedCue = expr.CueNumber;
        }
    }
}
