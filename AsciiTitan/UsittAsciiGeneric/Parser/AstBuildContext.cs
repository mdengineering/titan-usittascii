﻿using System;
using System.Collections.Generic;

namespace UsittAsciiGeneric.Parser
{
    public class AstBuildContext
    {
        public string Manufacturer
        {
            get;
            private set;
        }
 
        public string Console
        {
            get;
            private set;
        }

        Dictionary<(string Manufactuer, string Console), IMaufactuerSpecificAstBuilder> manufactuerHelpers
        = new Dictionary<(string Manufactuer, string Console), IMaufactuerSpecificAstBuilder>();

        public AstBuildContext()
        {
        }

        internal void SetManufacturer(string name)
        {
            Manufacturer = name;
        }

        internal void SetConsole(string name)
        {
            Console = name;
        }

        internal object EvalManufacturerSeconday(SyntaxTree syntaxTree, ParseTree tree, object[] paramlist)
        {
            IMaufactuerSpecificAstBuilder builder;
            if(manufactuerHelpers.TryGetValue((Manufacturer, Console), out builder))
            {
                return builder.EvalManufacturerSeconday(syntaxTree, tree, paramlist);
            }
            else
            {
                return null;
            }
        }

        internal object EvalManufacturerPrimary(SyntaxTree syntaxTree, ParseTree tree, object[] paramlist)
        {
			IMaufactuerSpecificAstBuilder builder;
			if (manufactuerHelpers.TryGetValue((Manufacturer, Console), out builder))
			{
				return builder.EvalManufacturerPrimary(syntaxTree, tree, paramlist);
			}
			else
			{
				return null;
			}
        }
    }
}
