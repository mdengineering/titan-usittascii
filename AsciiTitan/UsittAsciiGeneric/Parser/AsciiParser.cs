﻿﻿using System;
using System.Collections.Generic;
using UsittAsciiGeneric.Expressions;
using System.Linq;

namespace UsittAsciiGeneric.Parser
{
    public class SyntaxTree : ParseTree
    {
        // must create public constructor for the SyntaxTree
        public SyntaxTree()
        {
        }

        // must implement the second constructor that accepts parameters to create the node
        // this will be used internally only, so protected is enough.
        protected SyntaxTree(Token token, string text)
        {
            Token = token;
            this.text = text;
            nodes = new List<ParseNode>();
        }

        public override ParseNode CreateNode(Token token, string text)
        {
            SyntaxTree node = new SyntaxTree(token, text);
            node.Parent = this;
            return node;
        }

        /// <summary>
        /// Evals the start.
        /// Start -> (WHITESPACE? (BasicCommand | PrimaryCommandBlock))* EOF;
        /// </summary>
        /// <returns>The start.</returns>
        /// <param name="tree">Tree.</param>
        /// <param name="paramlist">Paramlist.</param>
        protected override object EvalStart(ParseTree tree, params object[] paramlist)
        {
            AstBuildContext context = GetContext(paramlist);
            if (context == null)
            {
                context = new AstBuildContext();
                paramlist = new object[] { context };
            }

            var expr = new AsciiFileExpression();
            foreach (var node in Nodes)
            {
                var expression = node.Eval(tree, paramlist) as Expression;
                if (expression != null)
                {
                    expr.Add(expression);
                }
            }

            return expr;
        }

        private static AstBuildContext GetContext(object[] paramlist)
        {
            return paramlist.Length > 1 ? paramlist[0] as AstBuildContext : null;
        }

        /// <summary>
        /// Evals the basic command.
        /// BasicCommand -> (Clear | Console | Ident | Manufacturer | Patch | Set | ManufacturerSecondary  )? COMMENT? EOL;
        /// </summary>
        /// <returns>The basic command.</returns>
        /// <param name="tree">Tree.</param>
        /// <param name="paramlist">Paramlist.</param>
        protected override object EvalBasicCommand(ParseTree tree, params object[] paramlist)
        {
            var node = Nodes[0];
            if (node.Token.Type == TokenType.COMMENT || node.Token.Type == TokenType.EOL)
            {
                // Nothing of interest here
                return null;
            }

            switch (node.Token.Type)
            {
                case TokenType.COMMENT:
                case TokenType.EOL:
                    // Nothing of interest here
                    return null;


                default:
                    return node.Eval(tree, paramlist);

            }
        }

        /// <summary>
        /// Evals the ident.
        /// Ident -> IDENT DELIMITER IdentVersion ;
        /// </summary>
        /// <returns>The ident.</returns>
        /// <param name="tree">Tree.</param>
        /// <param name="paramlist">Paramlist.</param>
        protected override object EvalIdent(ParseTree tree, params object[] paramlist)
        {
            var expression = new IdentCommandExpression();
            expression.Version = (Version)Nodes[2].Eval(tree, paramlist);
            return expression;
        }

        /// <summary>
        /// Evals the ident version.
        /// IdentVersion -> INTEGER COLON INTEGER;
        /// </summary>
        /// <returns>The ident version.</returns>
        /// <param name="tree">Tree.</param>
        /// <param name="paramlist">Paramlist.</param>
        protected override object EvalIdentVersion(ParseTree tree, params object[] paramlist)
        {
            return new Version((int)Nodes[0].Eval(tree, paramlist), (int)Nodes[2].Eval(tree, paramlist));
        }

        protected override object EvalInteger(ParseTree tree, params object[] paramlist)
        {
            // Because the parser doesn't have much lookahead and the delimiters are so vague this often happens
            if (Nodes[0].Token.Type == TokenType._UNDETERMINED_)
                return null;

            return int.Parse(Nodes[0].Token.Text);
        }

        /// <summary>
        /// Evals the clear.
        /// Clear -> CLEAR DELIMITER (CLEARTARGET | MANUFACTURER_PRIMARYKEYWORD );
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalClear(ParseTree tree, params object[] paramlist)
        {
            return new ClearCommandExpression() { Target = Nodes[2].Token.Text.Trim() };
        }

        /// <summary>
        /// Evals the set.
        /// Set -> SET DELIMITER((PATCH DELIMITER DEFAULT) | ((SETTARGET | MANUFACTURER_PRIMARYKEYWORD ) (DELIMITER (Integer | STRING))?));
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalSet(ParseTree tree, params object[] paramlist)
        {
            string value = null;
            if (Nodes.Count >= 5)
            {
                value = Nodes[4].Token.Text;
            }
            return new SetCommandExpression() { Target = Nodes[2].Token.Text, Value = value };
        }

        /// <summary>
        /// Evals the manufacturer.
        /// Manufacturer -> MANUFACTURER DELIMITER STRING;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalManufacturer(ParseTree tree, params object[] paramlist)
        {
            var expr = new SetManufacturerConextCommandExpression() { Name = Nodes[2].Token.Text.Trim() };

            AstBuildContext context = GetContext(paramlist);
            if(context != null)
            {
                context.SetManufacturer(expr.Name);
            }

            // Do we need to return this if it's absorbed at AST build??
            return expr;
        }

        /// <summary>
        /// Evals the console.
        /// Console -> CONSOLE DELIMITER STRING;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalConsole(ParseTree tree, params object[] paramlist)
        {
            var expr = new SetConsoleContextCommandExpression() { Name = Nodes[2].Token.Text.Trim() };

			AstBuildContext context = GetContext(paramlist);
			if (context != null)
			{
				context.SetConsole(expr.Name);
			}


            // Do we need to return this if it's absorbed at AST build??
            return expr;
        }


		/// <summary>
		/// Evals the primary command block.
		/// </summary>
		/// <param name="tree">The tree.</param>
		/// <param name="paramlist">The paramlist.</param>
		/// <returns></returns>
		protected override object EvalPrimaryCommandBlock(ParseTree tree, params object[] paramlist)
        {
            return Nodes[0].Eval(tree, paramlist);
        }

        /// <summary>
        /// Evals the primary command.
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalPrimaryCommand(ParseTree tree, params object[] paramlist)
        {
            return Nodes[0].Eval(tree, paramlist);
        }

        /// <summary>
        /// Evals the cue block.
        /// CueBlock -> Cue COMMENT? EOL (CuePartBlock+ | (BasicCommand | CuePartCommamd)*);
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalCueBlock(ParseTree tree, params object[] paramlist)
        {
            CueCommandExpression cue = new CueCommandExpression();
			Decimal cueNumber = (Decimal)Nodes[0].Nodes[2].Eval(tree, paramlist);
			cue.CueNumber = cueNumber;
			EvalCuePartBlock(tree, paramlist, cue);
            return cue;
        }

        private void EvalCuePartBlock(ParseTree tree, object[] paramlist, PrimaryCommandExpression cue)
        {

            foreach (var childNode in Nodes)
            {
                switch (childNode.Token.Type)
                {
                    // Maybe add these differently?
                    case TokenType.BasicCommand:
                    case TokenType.CuePartBlock:
                    case TokenType.CuePartCommamd:
                        cue.Add((Expression)childNode.Eval(tree, paramlist));
                        break;
                }
            }
        }

        protected override object EvalCueNumber(ParseTree tree, params object[] paramlist)
        {
            return Decimal.Parse(Nodes[0].Token.Text);
        }

        /// <summary>
        /// Evals the cue part commamd.
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalCuePartCommamd(ParseTree tree, params object[] paramlist)
        {
            return Nodes[0].Eval(tree, paramlist);
        }

        protected override object EvalGroupPartCommamd(ParseTree tree, params object[] paramlist)
        {
            return Nodes[0].Eval(tree, paramlist);
        }

        protected override object EvalSubPartCommamd(ParseTree tree, params object[] paramlist)
        {
            return Nodes[0].Eval(tree, paramlist);
        }

        /// <summary>
        /// Evals up.
        /// Up -> UP DELIMITER TIME (DELIMITER TIME)?;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalUp(ParseTree tree, params object[] paramlist)
        {
            UpExpression expr = new UpExpression();
            expr.FadeTime = (TimeSpan?)Nodes[2].Eval(tree, paramlist);

            if (Nodes.Count >= 5)
            {
                expr.DelayTime = (TimeSpan?)Nodes[4].Eval(tree, paramlist);
            }

            return expr;
        }

        protected override object EvalDown(ParseTree tree, params object[] paramlist)
        {
            DownExpression expr = new DownExpression();
            expr.FadeTime = (TimeSpan?)Nodes[2].Eval(tree, paramlist);

            if (Nodes.Count >= 5)
            {
                expr.DelayTime = (TimeSpan?)Nodes[4].Eval(tree, paramlist);
            }

            return expr;
        }

        protected override object EvalTime(ParseTree tree, params object[] paramlist)
        {
            if (Nodes[0].Token.Type == TokenType._UNDETERMINED_)
                return null;

            string nodeText = Nodes[0].Token.Text;

            return ParseTime(nodeText);
        }

        public static TimeSpan ParseTime(string text)
        {
            var parts = text.Split(':');

            TimeSpan time = TimeSpan.Zero;

            if (parts.Length > 0)
            {
                time = TimeSpan.FromSeconds(float.Parse(parts[parts.Length - 1]));
            }
            if (parts.Length > 1)
            {
                time += TimeSpan.FromMinutes(float.Parse(parts[parts.Length - 2]));
            }
            if (parts.Length > 2)
            {
                time += TimeSpan.FromHours(float.Parse(parts[parts.Length - 3]));
            }

            return time;
        }

        /// <summary>
        /// Evals the channel.
        /// Channel -> CHAN (DELIMITER Integer DELIMITER Level)+;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalChannel(ParseTree tree, params object[] paramlist)
        {
            ExpressionGroup group = new ExpressionGroup();
            for (int i = 2; i < Nodes.Count; i += 4)
            {
                int? channel = (int?)Nodes[i].Eval(tree, paramlist);
                if (channel.HasValue)
                {
                    float? level = (float?)Nodes[i + 2].Eval(tree, paramlist);
                    if (level.HasValue)
                    {
                        group.Expressions.Add(new ChannelExpression() { Channel = channel.Value, Level = level.Value });
                    }
                }
            }
            return group;
        }

        /// <summary>
        /// Evals the level.
        /// Level -> Integer | Hex;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalLevel(ParseTree tree, params object[] paramlist)
        {
            int? value = (int?)Nodes[0].Eval(tree, paramlist);

            if (value.HasValue)
            {
                if (Nodes[0].Token.Type == TokenType.Integer)
                {
                    return (float)value / 100f;
                }
                else
                {
                    return (float)value / 255f;
                }
            }

            return null;
        }

        /// <summary>
        /// Evals the hexadecimal.
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalHex(ParseTree tree, params object[] paramlist)
        {
            if (Nodes[0].Token.Type == TokenType._UNDETERMINED_)
                return null;

            string stringValue = Nodes[0].Token.Text.Trim().Replace("h", string.Empty).Replace("H", string.Empty);
            return int.Parse(stringValue, System.Globalization.NumberStyles.HexNumber);
        }

        /// <summary>
        /// Evals the text.
        /// Text -> TEXT DELIMITER ANYSTUFF;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalText(ParseTree tree, params object[] paramlist)
        {
            return new TextExpression() { Text = Nodes[2].Token.Text.Trim() };
        }

        /// <summary>
        /// Evals the follow on.
        /// FollowOn -> FOLLOWON DELIMITER Time;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalFollowOn(ParseTree tree, params object[] paramlist)
        {
            return new FollowOnExpression() { Time = (TimeSpan) Nodes[2].Eval(tree, paramlist) };
        }

        /// <summary>
        /// Evals the link.
        /// Link -> LINK DELIMITER CueNumber;
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="paramlist">The paramlist.</param>
        /// <returns></returns>
        protected override object EvalLink(ParseTree tree, params object[] paramlist)
        {
            return new LinkExpression() { CueNumber = (Decimal)Nodes[2].Eval(tree, paramlist) };
        }

		/// <summary>
		/// Evals the cue part block.
		/// CuePartBlock -> Part COMMENT? EOL (BasicCommand | CuePartCommamd)*;
		/// </summary>
		/// <param name="tree">The tree.</param>
		/// <param name="paramlist">The paramlist.</param>
		/// <returns></returns>
		protected override object EvalCuePartBlock(ParseTree tree, params object[] paramlist)
        {
            var partExpr = new CuePartExpression();
            int partNumber = ((int?)(Nodes[0].Nodes[2].Eval(tree, paramlist))).Value;
			partExpr.PartNumber = partNumber;
			EvalCuePartBlock(tree, paramlist, partExpr);
            return partExpr;
        }

        protected override object EvalManufacturerSecondary(ParseTree tree, params object[] paramlist)
        {
            AstBuildContext context = GetContext(paramlist);
            if(context != null)
            {
                return context.EvalManufacturerSeconday(this, tree, paramlist);
            }
            return null;
        }

        protected override object EvalManufacturerBlock(ParseTree tree, params object[] paramlist)
        {
			AstBuildContext context = GetContext(paramlist);
			if (context != null)
			{
				return context.EvalManufacturerPrimary(this, tree, paramlist);
			}
			return null;
        }

		/// <summary>
		/// Evals the group block.
        /// GroupBlock -> Group COMMENT? EOL (GroupPartBlock+ | (BasicCommand | GroupPartCommamd)*);
		/// </summary>
		/// <returns>The group block.</returns>
		/// <param name="tree">Tree.</param>
		/// <param name="paramlist">Paramlist.</param>
		protected override object EvalGroupBlock(ParseTree tree, params object[] paramlist)
        {
			var expr = new GroupCommandExpression();
            Decimal number = ((Decimal)(Nodes[0].Nodes[2].Eval(tree, paramlist)));
            expr.Number = number;
			EvalGroupPartBlock(tree, paramlist, expr);
			return expr;
        }


		private void EvalGroupPartBlock(ParseTree tree, object[] paramlist, PrimaryCommandExpression cue)
		{

			foreach (var childNode in Nodes)
			{
				switch (childNode.Token.Type)
				{
					// Maybe add these differently?
					case TokenType.BasicCommand:
                    case TokenType.GroupPartBlock:
                    case TokenType.GroupPartCommamd:
						cue.Add((Expression)childNode.Eval(tree, paramlist));
						break;
				}
			}
		}


        protected override object EvalSubBlock(ParseTree tree, params object[] paramlist)
        {
            var partExpr = new SubCommandExpression();
			int partNumber = ((int?)(Nodes[0].Nodes[2].Eval(tree, paramlist))).Value;
			partExpr.Number = partNumber;
			EvalSubPartBlock(tree, paramlist, partExpr);
			return partExpr;      
        }

		private void EvalSubPartBlock(ParseTree tree, object[] paramlist, PrimaryCommandExpression cue)
		{

			foreach (var childNode in Nodes)
			{
				switch (childNode.Token.Type)
				{
					// Maybe add these differently?
					case TokenType.BasicCommand:
                    case TokenType.SubPartCommamd:
						cue.Add((Expression)childNode.Eval(tree, paramlist));
						break;
				}
			}
		}

		/// <summary>
		/// Evals the patch.
		/// Patch -> PATCH DELIMITER Integer (DELIMITER Integer DELIMITER Integer DELIMITER Level)*;
		/// </summary>
		/// <returns>The patch.</returns>
		/// <param name="tree">Tree.</param>
		/// <param name="paramlist">Paramlist.</param>
		protected override object EvalPatch(ParseTree tree, params object[] paramlist)
        {
            int page = ((int?)Nodes[2].Eval(tree, paramlist)).Value;


			ExpressionGroup group = new ExpressionGroup();
			for (int i = 4; i < Nodes.Count; i += 6)
			{
				int? channel = (int?)Nodes[i].Eval(tree, paramlist);
				if (channel.HasValue)
				{

					int? address = (int?)Nodes[i + 2].Eval(tree, paramlist);
                    if (address.HasValue)
                    {
                        float? level = (float?)Nodes[i + 4].Eval(tree, paramlist);
                        if (level.HasValue)
                        {
                            group.Expressions.Add(new PatchCommandExpression() { Page = page, Address = address.Value, Channel = channel.Value, Level = level.Value });
                        }
                    }
				}
			}
			return group;
        }


        protected override object EvalEndData(ParseTree tree, params object[] paramlist)
        {
            return null;
        }
	}
}
