﻿using System;

namespace UsittAsciiGeneric.Parser
{
    public interface IMaufactuerSpecificAstBuilder
    {
        object EvalManufacturerSeconday(SyntaxTree syntaxTree, ParseTree tree, object[] paramlist);
        object EvalManufacturerPrimary(SyntaxTree syntaxTree, ParseTree tree, object[] paramlist);
    }
}