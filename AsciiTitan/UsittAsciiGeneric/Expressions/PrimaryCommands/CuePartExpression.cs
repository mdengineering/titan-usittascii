﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class CuePartExpression : PrimaryCommandExpression
    {
        public int PartNumber { get; internal set; }

		public override void Execute(Data.DataManager dataManager)
		{
			// Parts not supported yet
            base.Execute(dataManager, Data.DataManager.EmptyContext);
		}

        public override void Execute(Data.DataManager dataManager, Data.PrimaryBase context)
        {
			// Parts not supported yet
			base.Execute(dataManager, Data.DataManager.EmptyContext);
        }
    }
}
