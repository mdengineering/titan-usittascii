﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsittAsciiGeneric.Data;

namespace UsittAsciiGeneric.Expressions
{
    class CueCommandExpression : PrimaryCommandExpression
    {
        public decimal CueNumber { get; internal set; }

        public override void Execute(Data.DataManager dataManager)
        {
            Cue cue = new Cue() { Number = CueNumber };
            dataManager.AddCue(cue);
            base.Execute(dataManager, cue);
        }
    }
}
