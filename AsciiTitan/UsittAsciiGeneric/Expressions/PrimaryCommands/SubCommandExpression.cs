﻿using System;
using UsittAsciiGeneric.Data;

namespace UsittAsciiGeneric.Expressions
{
    public class SubCommandExpression : PrimaryCommandExpression
    {
        public SubCommandExpression()
        {
        }

        public int Number { get; internal set; }

		public override void Execute(Data.DataManager dataManager)
		{
			Sub sub = new Sub() { Number = Number };
			dataManager.AddSub(sub);
			base.Execute(dataManager, sub);
		}
    }
}
