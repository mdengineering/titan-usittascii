﻿using System;
using UsittAsciiGeneric.Data;

namespace UsittAsciiGeneric.Expressions
{
    public class GroupCommandExpression : PrimaryCommandExpression
    {
        public GroupCommandExpression()
        {
        }

        public decimal Number { get; internal set; }

		public override void Execute(Data.DataManager dataManager)
		{
			Group group = new Group() { Number = Number };
			dataManager.AddGroup(group);
            base.Execute(dataManager, group);
		}

    }
}
