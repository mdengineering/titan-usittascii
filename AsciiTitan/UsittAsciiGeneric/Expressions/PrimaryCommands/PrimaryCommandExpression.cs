﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    public class PrimaryCommandExpression : Expression
    {
        List<Expression> expresions = new List<Expression>();

        public IReadOnlyList<Expression> Children { get { return expresions; } }

        internal void Add(Expression v)
        {
            if (v != null)
            {
                if (v is ExpressionGroup)
                {
                    foreach (var child in ((ExpressionGroup)v).Expressions)
                    {
                        Add(child);
                    }
                }
                else
                {
                    expresions.Add(v);
                }
            }
        }

        public override void Execute(Data.DataManager dataManager) => Execute(dataManager, null);

        public virtual void Execute(Data.DataManager dataManager, Data.PrimaryBase context)
        {
          
            foreach(var child in Children)
            {
                switch(child)
                {
                    case CuePartExpression e:
						e.Execute(dataManager, context);
						break;

					case SecondaryExpression e:
                        e.Execute(dataManager, context);
                        break;

                    case Expression e:
                        e.Execute(dataManager);
                        break;
                }

            }
        } 
    }
}
