﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class SetCommandExpression : Expression
    {
        public string Target { get; internal set; }
        public string Value { get; internal set; }
    }
}
