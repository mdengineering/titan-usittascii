﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class ClearCommandExpression : Expression
    {
        public string Target { get; set; }
    }
}
