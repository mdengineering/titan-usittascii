﻿using System;
namespace UsittAsciiGeneric.Expressions
{
    public class PatchCommandExpression : Expression
    {
        public PatchCommandExpression()
        {
        }

        public int Page { get; internal set; }
        public int Address { get; internal set; }
        public int Channel { get; internal set; }
        public float Level { get; internal set; }

        public override void Execute(Data.DataManager dataManager)
        {
            dataManager.Patch(Channel, Address, Level);
            base.Execute(dataManager);
        }
    }
}
