﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class ExpressionGroup : Expression
    {
        public List<Expression> Expressions { get; set; } = new List<Expression>();

    }
}
