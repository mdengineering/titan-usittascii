﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class ChannelExpression : SecondaryExpression
    {

        public float Level { get; set; }
        public int Channel { get; internal set; }

		public override void Execute(Data.DataManager dataManager, Data.PrimaryBase context)
		{
			context.Apply(this);
			base.Execute(dataManager, context);
		}
    }
}
