﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsittAsciiGeneric.Expressions
{
    class TextExpression : SecondaryExpression
    {
        public String Text { get; set; }

		public override void Execute(Data.DataManager dataManager, Data.PrimaryBase context)
		{
			context.Apply(this);
			base.Execute(dataManager, context);
		}
    }
}
