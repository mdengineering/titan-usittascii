﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsittAsciiGeneric.Data;

namespace UsittAsciiGeneric.Expressions
{
    class UpExpression : SecondaryExpression
    {
        public TimeSpan? FadeTime { get; set; }
        public TimeSpan? DelayTime { get; set; }

		public override void Execute(Data.DataManager dataManager, Data.PrimaryBase context)
		{
			context.Apply(this);
            base.Execute(dataManager, context);
		}
    }
}
