﻿using System;
using System.Collections.Generic;
using System.IO;
using Mono.Options;
using UsittAcsii.Titan;
using UsittAsciiGeneric.Data;
using UsittAsciiGeneric.Expressions;
using UsittAsciiGeneric.Parser;
using System.Net;
using System.Reflection;

namespace UsittAsciiTestConsole
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            bool showHelp = false;
            string filename = null;
            bool printTree = false;
            bool sendToTitan = false;
            var importOptions = new ImportOptions();
            OptionSet options = new OptionSet()
            {
                { "f|file=", "name of a file to load", f => filename = f},
                { "h|help", "show this message and exit", h => showHelp = h != null },
                { "printTree", "print the complete parse tree of the input file", h => printTree = h != null },
                { "sendToTitan", "send the show to Titan", h => sendToTitan = h != null },
                { "n|newShow", "when sending the show to Titan start a new show (Default on)", h => importOptions.StartNewShow = h != null },
                { "unpatchedChannels", "what to do with channels that are used in subs, groups and cues that aren't in a Patch command. Options are Skip, PatchParked, PatchAtChannel",
                 h => { importOptions.UnpatchedChannelBehaviour = (ImportOptions.UnpatchedChannels)Enum.Parse(typeof(ImportOptions.UnpatchedChannels), h as string); } },
                { "consoleAddress", "the IP address of the Titan engine to connect to. Defaults to localhost", h => importOptions.Address = h }
            };

			List<string> extra;
			try
			{
				// parse the command line
				extra = options.Parse(args);
			}
			catch (OptionException e)
			{
				// output some error message
				Console.Write("asciiImporter: ");
				Console.WriteLine(e.Message);
				Console.WriteLine("Try `asciiImporter --help' for more information.");
				return;
			}

            if(showHelp)
            {
				Console.WriteLine("asciiImporter: ");
                Console.WriteLine("Version: " + Assembly.GetExecutingAssembly().GetName().Version);
                Console.WriteLine("Usage: asciiImporter.exe [OPTIONS]+");
				Console.WriteLine("Test harness to read USITT ACSII lighting show files.");
				Console.WriteLine();

				// output the options
				Console.WriteLine("Options:");
				options.WriteOptionDescriptions(Console.Out);

                return;
            }

            if(!File.Exists(filename))
            {
                Console.WriteLine("asciiImporter: ");
                Console.WriteLine($"File '{filename}' doesn't exist!");
				Console.WriteLine("Try `asciiImporter --help' for more information.");
				return;
            }

            string wholeFile = File.ReadAllText(filename);
            Scanner scanner = new Scanner();
            Parser parser = new Parser(scanner);
            SyntaxTree tree = new SyntaxTree();
            parser.Parse(wholeFile, tree);

            if (printTree)
            {
                Console.WriteLine(tree.PrintTree());
            }

            AsciiFileExpression expr = (AsciiFileExpression) tree.Eval();
            DataManager data = new DataManager();
            expr.Execute(data);

            if(sendToTitan)
            {
                Importer importer = new Importer();
                importer.BuildShow(data, importOptions);
            }

        }
    }
}
