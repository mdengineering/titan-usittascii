SET InstallDir=%PROGRAMFILES(X86)%\Avolites\Titan Simulator\

xcopy "%InstallDir%Avolites.Acw.dll" "." /DYF
xcopy "%InstallDir%Avolites.Acw.Titan*.dll" "." /DYF
xcopy "%InstallDir%Avolites.General.Utilities.dll" "." /DYF
xcopy "%InstallDir%Avolites.Menus.dll" "." /DYF
xcopy "%InstallDir%Avolites.General.ExceptionHandling.dll" "." /DYF