﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UsittAsciiGeneric.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsittAsciiGeneric.Expressions;

namespace UsittAsciiGeneric.Parser.Tests
{
    [TestClass()]
    public class SyntaxTreeTests
    {
        [TestMethod()]
        public void ParseTimeTest()
        {
            Assert.AreEqual<TimeSpan>(TimeSpan.FromSeconds(1), SyntaxTree.ParseTime("1"));
            Assert.AreEqual<TimeSpan>(TimeSpan.FromSeconds(2.76), SyntaxTree.ParseTime("2.76"));
            Assert.AreEqual<TimeSpan>(TimeSpan.FromMinutes(1.5), SyntaxTree.ParseTime("1:30"));
            Assert.AreEqual<TimeSpan>(new TimeSpan(0, 0, 6, 45, 10), SyntaxTree.ParseTime("6:45.01"));
            Assert.AreEqual<TimeSpan>(new TimeSpan(0, 1, 0, 0, 0), SyntaxTree.ParseTime("01:0:0"));
        }

        [TestMethod()]
        public void Text()
        {
            string exampleText = "CUE 5\nTEXT this is the tile   ! Some comment\n";
            CueCommandExpression cue = ParseCueString(exampleText);
            TextExpression text = (TextExpression)cue.Children.First();
            Assert.AreEqual("this is the tile", text.Text);
        }

        [TestMethod()]
        public void IntLevels()
        {
            string exampleText = "CUE 5\nChan 1=50  2=30  5=100  6=100   ! Some comment\n";
            CueCommandExpression cue = ParseCueString(exampleText);
            Assert.AreEqual<float>(0.5f, ((ChannelExpression)cue.Children[0]).Level);
            Assert.AreEqual<float>(0.3f, ((ChannelExpression)cue.Children[1]).Level);
            Assert.AreEqual<float>(1f, ((ChannelExpression)cue.Children[2]).Level);
            Assert.AreEqual<float>(1f, ((ChannelExpression)cue.Children[3]).Level);
        }

        private static CueCommandExpression ParseCueString(string exampleText)
        {
            AsciiFileExpression expr = ParseString(exampleText);

            CueCommandExpression cue = (CueCommandExpression)expr.Children.First();
            return cue;
        }

        private static AsciiFileExpression ParseString(string exampleText)
        {
            Scanner scanner = new Scanner();
            Parser parser = new Parser(scanner);
            SyntaxTree tree = new SyntaxTree();
            parser.Parse(exampleText, tree);

            AsciiFileExpression expr = tree.Eval() as AsciiFileExpression;
            return expr;
        }
    }
}