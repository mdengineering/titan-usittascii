
!                Sample USITT ASCII Cue file with LP90 extensions
!                ------------------------------------------------

!    An ASCII Cue file contains a list of instructions which
!    describe cues, groups, submasters, dimmer softpatch, and other
!    information in a lighting console.  The instructions in the file are
!    very similar to those an operator might use to create the show at the
!    console.

!    This file is an actual ASCII Cue text file with extensive comments
!    which explain each instruction.  The file includes examples of cues,
!    groups, submasters, and softpatch.  There are also examples of "manufacturer
!    specific" commands developed to support the Strand Lightpalette 90
!    features including data for effects, macros and profiles.

!  Some basics:
!    -- Instructions consist of words and numbers separated by "delimiters".
!       All delimiters are interchangeable. Anywhere this sample file uses
!       a space, comma, >, <, =, @, or / it may be replaced by one or more of
!       any combination of the other delimiters.
!
!    -- No line in the file can be longer than 80 characters, including
!       all delimiters and spaces. Blank lines are ok.
!
!    -- Capitalization is unimportant. CUE, Cue and cUe are all the same.
!
!    -- Only one instruction on a line, please.
!
!    -- For reasons beyond the scope of this sample file, the number of "$"
!       symbols in special LP90 instructions is important.
!
! << An exclamation point indicates that the rest of the line is a
!    comment about the file, and not information to be processed.


!========= Basic Instructions ==========

Ident 3:0
!
!    The IDENT instruction identifies the version of the ASCII Cues
!    language used in a file. It is an optional instruction. If present,
!    it must be the first instruction in the file.

Clear Cues
Clear Submasters
Clear Groups
Clear Patch
Clear All
!
!    The CLEAR instruction clears any data in the console before
!    loading the information contained in the text file.  CLEAR ALL
!    does the work of the four preceding CLEAR instructions.

Set Channels 100
Set Dimmers 100
Set Patch Default
!
!    The SET instruction defines basic show information.
!    Default patch is dimmer 1 = channel 1, etc.  Use of all SET
!    instructions is optional.

Manufacturer Strand
Console Lightpalette90
!
!    These two instructions enable processing of LP90 information
!    (effects, macros, profiles, etc.)  Consoles or translators which do 
!    not have LP90 capability will simply ignore all LP90-specific
!    instructions.  No $ or $$ instructions will be processed until 
!    after the above instructions enable the LP90 mode.
!
!    After the LP90 mode is enabled, special basic instructions may be used.

Set $Title SAMPLE SHOW
!
!    The SET $TITLE instruction defines the show name.

Clear $Effects
Clear $Macros
Clear $Profiles
!
!    Special versions of the CLEAR instruction clear any or all of the
!    LP90-specific data areas.  All LP90 data areas are cleared by CLEAR ALL.


!========= Cues ==========

Cue 1
!
!    The CUE instruction marks the beginning of a cue description.  The
!    following instructions define fade times and levels for cue 1.

Up 5
Down 10
!
!    The UP and DOWN instructions set the fade time for the cue.
!    If only one direction is listed, the other direction has the
!    same time.

Chan 1=50  2=30  5=100  6=100
!
!    The CHAN instruction sets levels for the cue. All channels which
!    are up in a cue must be listed in a CHAN instruction. Any channels not
!    listed are set to zero. Multiple CHAN instructions may be used to set
!    many channel levels in one cue.

Chan 10@h80
!
!    This instruction, typical of ETC ASCII Cue support, uses @ as a level delimiter,
!    and lists the actual level as a hexadecimal value instead of a decimal percentage.
!    h00 is 0, h40 is 25%, h80 is 50%, HC0 is 75% and hFF is full.

Text curtain warmers
!
!    The TEXT instruction attaches descriptive comments to a cue.
!    Cue 1 will get the comment "curtain warmers".

Followon 15
!
!    The FOLLOWON instruction sets an optional automatic follow time (WAIT).
!    The next cue will start 15 seconds after cue 1 starts.

Link 8.5
!
!    The LINK instruction changes the cue sequence. Cue 8.5 will
!    be loaded after cue 1.

!========= Cue Variations ==========

!    This cue shows the use of "point" cue numbers, delayed fades,
!    and minutes and seconds time format.

Cue 2.3           ! Cue numbers range from 0.1 to 999.9
Up 120 20         ! The 2 minute up time is delayed by 20 seconds
Down 1:15         ! Time may also be written as hours:minutes:seconds
Chan 1 100 5 25   ! Channels 2, 6 and 10 will go out since they are not listed

!========= Part Cues ==========

Cue 8.5
Part 1
!
!    The PART instruction allows multi-part cues to be defined.

Up 3                ! (Down time of 3 is implied)
Chan 2 100 3 100
Part 2              ! Start of second part description for cue 8.5
Up 3 1              ! 3 second fade, delayed by 1 second
Chan 1 50 7 50 8 75 ! Channel 5 will go out (since it is not listed)
                    !   in part 2 (the last part).

!========= LP90 Cue Features ==========

Cue 10
Up 999
$$Effect 1          ! Assigns effect 1 to cue 10.  
Chan 1 50 2 100     ! Effect cues may not change channel levels, so CHAN
Chan 3 100 7 50 8 75!   instructions from previous cue must be repeated,
                    !   otherwise the cue would indicate a fade to black.

Cue 11
$$Profile 3         ! Profile 3 is assigned to cue 11

Cue 12
$$Macro 1           ! Cue 12 executes macro 1

Cue 13
Chan 1 50 2 50 3 100
$$Remote 7.5        ! Cue 13 activates remote cue 7.5
Link 12
$$LoopCount 7       ! Console will execute the cue link 7 times

Cue 14
Part 1              ! Part 1...
Up 2
Chan 1 100          !          ...fades channel 1 from 50 to full.
Part 2              ! Part 2...
Up 5
Chan 2 100          !          ...fades channel 2 from 50 to full.
$$Blocked           ! The BLOCKED instruction forces each following
Chan 4 0            !   channel to have a "move" to the listed level,
                    !   even though it may already be at that level.

$$Tracking          ! The TRACKING instruction indicates that the
Chan 3 100          !   following channels are not blocked.

                    ! In a multi-part cue, LightPort always lists tracking
                    !   channels after the last part. If the last part has
                    !   $$BLOCKED channels, the tracking channels must be
                    !   preceded by a $$TRACKING instruction.



!=========  Groups  ==========

Group 10            ! Start of description of group 10.  Groups
                    !   may be numbered 1 to 999 .
Chan 1 100 2 100    ! CHAN instructions set levels in the group.
Text blue wash      ! Optional group name may be assigned.

!========= Submasters ==========

Sub 1               ! Start of submaster 1 description.  Subs may
                    !   be numbered 1 to 48.

Chan 10 100 11 50   ! Any number of CHAN instructions may be used to
                    !   set levels in the submaster.

Sub 2
$$Inhibit           ! Sub is inhibitive, otherwise it is pile-on
$$OverRange         ! Sub is over-range, otherwise it is normal
$$NoBump            ! Bump button is disabled
Chan 1 20 2 20 5 80

Sub 3
$$LoadGroup 10      ! Sub 3 has group 10 loaded
$$BumpInd           ! Bump button toggles dependent/independent mode

Sub 4
$$LoadEffect 2      ! Sub 4 has effect 2 loaded
$$BumpOut           ! Bump button takes sub out

Sub 5
$$BumpQuickLoad     ! Sub 5 bump button does quick-load


!========= Softpatch ==========

Patch 1 5<8@100 2<9@100 3<10@70 4<11@43
!
!    PATCH instructions define the dimmer-to-channel softpatch. The ASCII
!    Cues format includes information for proportional dimmer levels and
!    multiple versions of patch information, called pages. Though the LP90
!    does not follow this model, LightPort includes the data in its files
!    for compatibility with other ASCII Cue systems.
!
!    This example patches two dimmers on patch page 1. Dimmer 8 is patched to
!    channel 5 and dimmer 9 is patched to channel 2. Both have a proportional
!    level of 100%.

$$DimmerNames 1 FRT 2 BACK 4 PK2
!
!    Dimmer names are assigned with the $$DIMMERNAMES instruction.
!    See the LP90 manual for rules on naming dimmers.  In this example
!    dimmer 1 is named "FRT", dimmer 2 is named "BACK", etc.  Multiple
!    instructions may be used to assign many dimmer names.

$$DimmerProfiles 1=1 2=1 90=7
!
!    Dimmer profiles are assigned with the $$DIMMERPROFILES instruction.
!    Multiple instructions may be used to assign many dimmer profiles.

$$Dimmer6k 85 86 87 88 89 90
!
!    Dimmers are assigned as CD80 6K/12K. Multiple instructions may be
!      used to set many 6K/12K dimmers.


!========= Effects ==========
$Effect 2           ! Start of effect 2 description.

Text Wild           ! Optional effect comment.

$$High 80           ! Optional default high level, otherwise 100%.
$$Low 20            ! Optional default low level, otherwise 0.

$$StepTime .2       ! Optional default step time, otherwise 0.1 .

$$Negative          ! Optional effect attributes may be set, with these six
$$Alternate         !  instructions, one per line.  Omitted attributes
$$Bounce            !  are set to OFF.
$$Random
$$Build
$$Reverse

$$Step 1            ! Start of definition for step 1

$$High 50           ! Optional high level, otherwise default is used.
$$Low 10            ! Optional low level, otherwise default is used.
$$Time 1            ! Optional step time, otherwise default is used.
$$InDwellOut 0 3 0  ! Optional fade-in, dwell, and fade-out times for step.
                    !   Otherwise default fade-in and fade-out are 0,
                    !   and dwell time matches step time.

$$Control 1+2+3     ! Control list of channels in effect step.

                    ! Skipped steps (step 2 in this example) are empty.

$$Step 3            ! Start of definition for step 3.
$$Control 20+30+40


$Effect 5           ! Start of new effect description
$$Step 1
$$Control 1+3+5     ! Multiple CONTROL instructions may be used, but LP90
$$Control 7+9       !   limits the list to 25 items.

$$Step 2
$$Control 5>10-8+Q16.5+G10=50+S3+E4
                    ! Any valid LP90 control list may be included.


!========= Macros ==========

$Macro 8            ! Start of macro description.
Text time 15 *      ! TEXT instruction(s) describe macro.  LP90 limits
                    !   macros to 33 buttons.


!========= Profiles ==========

$Profile 5          ! Start of description for profile 5.

$$Curve 40=60 80=100! CURVE lists "corners" of profile curve; missing points
                    !   are automatically computed, as with LP90
                    !   "fill-in-blanks" feature.  Additional CURVE
                    !   instructions may be used as needed.


!========= The End ==========

EndData             ! For tidiness, the optional ENDDATA instruction
                    !   indicates the end of an ASCII Cues file.
					