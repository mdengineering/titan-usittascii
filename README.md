# Titan USITT ACSII Importer #

This software allows you to import USITT ACSII files into the Avolites Titan lighting software. It is intended to run on a laptop connected to the lighting network not on the console itself. Binary and Source code versions are provided.

## Limitations ##

This software is in an early stage of development. 

#### Supported ####
* Create cues (as a cue list), subs (as memories) and groups (as groups)
* Times and channel levels on cues and subs
* Links (via macros) and Followons in cues
* Patching including proportional patch (approximated with curves)
* Connect to Titan Simulator or Mobile locally
* Connect to a console over the network

#### Not supported (yet) ####
* Part cues
* Any manufacturer specific commands
* Merge with an exisiting show
* Creating handles (everything just appears in show library)

There is currently no GUI you must run the software from the command line.

## Binary Install ##

* Get a Windows 10 laptop and install Avolites Titan Simulator V10.1 onto it.
* Download the latest binary and unzip it somewhere. 
* Run the script 'GrabDlls.cmd'

## Binary Use ##
* If you want to import locally then run Titan Simulator, if you want to import direct to a console ensure the console is connected to the same network as the laptop.
* Open a shell (eg PowerShell, Cygwin or cmd)
* Run asciiImporter.exe with the following options
    * `--file=<filename>` Path and name of the ascii file
    * `--consoleAddress=<ipaddress>` IP of the console if you want to go direct to the console. In a multi console system this should be the IP of the master.
    * `--newShow` To start a new show
    * `--sendToTitan` To actually do it
* There are more options you can try, do --help to find out what they are.

## Building from source ##
* Clone the repo
* If you're on Windows with Titan Simulator installed then run the GrabDlls command in the Libraries directory. If you're not acquire those dlls some other way.
* After that open the solution and it should build fine in Visual Studio 2017 and Visual Studio for Mac. It runs fine on Mac but the mono version of WCF isn't quite good enough for it to actually work with Titan.
* The projects are
    * `Examples` Some example files and the language spec. Hopefully more coming if some users submit some.
    * `AsciiTitan` WPF Front End - Not yet started. Considering switching this for some other GUI toolkit to make it easier to develop on Mac.
    * `UsittAscii.Titan` Back end that takes an abstract representation of a show created from the ASCII file and pushes it into Titan. This is the only Titan specific bit of the project and a reasonable example of how to write a small program that talks to Titan via the WCF interface.
    * `UsittAsciiGeneric` Contains the grammar, parser, scanner, abstract syntax tree expressions for the USITT ACSII files. Also contains an generic representation of the show data in the `Data` namespace.
    * `UsittAsciiGenericTests` Some unit tests for various parsing functions. Needs some integration tests for real files. Doesn't work in Visual Studio for Mac.
    * `UsittAsciiTestConsole` Command line project, intended for testing but is the only currently functioning front end.

## Bugs and feature requests ##
* If you find a bug or need something to be imported that isn't currently supported please create an issue and *attach an example file* with an explanation of what it should look like.

## License ##
The software is provided under the MIT License. See LICENSE.md.